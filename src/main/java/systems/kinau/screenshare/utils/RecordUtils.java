package systems.kinau.screenshare.utils;

import javafx.application.Platform;
import lombok.Getter;
import org.apache.commons.lang3.RandomStringUtils;
import org.controlsfx.dialog.ProgressDialog;
import systems.kinau.screenshare.ScreenShare;
import systems.kinau.screenshare.monitor.SavingMonitor;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class RecordUtils extends TimerTask {

    @Getter private int timeBetweenPictures;
    @Getter private ScreenShare screenShare;
    @Getter private Rectangle section;
    @Getter private List<BufferedImage> images = new CopyOnWriteArrayList<>();
    @Getter private boolean running = false;

    private Timer timer;

    public void startRecord(ScreenShare screenShare, int timeBetweenPictures, Rectangle section) {
        this.screenShare = screenShare;
        this.timeBetweenPictures = timeBetweenPictures;
        this.section = section;

        this.running = true;
        this.timer = new Timer();
        this.timer.scheduleAtFixedRate(this, 0, timeBetweenPictures);
    }

    public void stop() {
        this.timer.cancel();
    }

    public void save(boolean showDialog) throws IOException {
        if(images.isEmpty())
            return;
        File file = new File(System.getenv("appdata") + "/ScreenShare/");
        file.mkdirs();
        screenShare.setLastUUID(UUID.randomUUID());
        screenShare.setLastIdentifier(RandomStringUtils.randomAlphanumeric(8));
        ImageOutputStream output = new FileImageOutputStream(new File(System.getenv("appdata") + "/ScreenShare/" + screenShare.getLastUUID().toString().replace("-","") + ".gif"));
        GifSequenceWriter gifSequenceWriter = new GifSequenceWriter(output, images.get(0).getType(), getTimeBetweenPictures(), true);

        SavingMonitor savingMonitor = new SavingMonitor(images.size());

        if(showDialog) {
            Platform.setImplicitExit(false);
            Platform.runLater(() -> {
                new ProgressDialog(savingMonitor);
                Thread thread = new Thread(savingMonitor);
                thread.setDaemon(true);
                thread.start();
            });
        }
        int counter = 0;
        for (BufferedImage image : getImages()) {
            gifSequenceWriter.writeToSequence(image);
            counter++;
            savingMonitor.setImagesSaved(counter);
        }
        gifSequenceWriter.close();
        output.close();
        images.clear();
        this.running = false;
    }

    @Override
    public void run() {
        images.add(screenShare.getScreenPicture(section));
    }
}
