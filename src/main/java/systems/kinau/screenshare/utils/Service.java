package systems.kinau.screenshare.utils;

import lombok.Getter;
import systems.kinau.screenshare.ScreenShare;

public enum Service {

    KINAU_SYSTEMS("192.168.178.100", "https://screens.kinau.systems/", "root", "", "/var/www/screens", System.getenv("userprofile") + "/.ssh/"),
    NEBORIA("neboria.eu", "https://screens.neboria.eu/", "root", "", "/var/www/screens", System.getenv("userprofile") + "/.ssh/"),
    DL_NEBORIA("neboria.eu", "https://download.neboria.eu/", "root", "", "/var/www/html/neboria/websites/general/download", System.getenv("userprofile") + "/.ssh/");

    @Getter private String baseURL, hostname, username, password, directory, sshDir;

    Service(String hostname, String baseURL, String username, String password, String directory, String sshDir) {
        this.hostname = hostname;
        this.baseURL = baseURL;
        this.username = username;
        this.password = password;
        this.directory = directory;
        this.sshDir = sshDir;
    }

    public UploadManager getUploadManager(SoundManager soundManager) {
        return new UploadManager(soundManager, this);
    }
}
