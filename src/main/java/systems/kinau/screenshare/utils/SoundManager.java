package systems.kinau.screenshare.utils;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class SoundManager {

	public void playSound(String sound) throws IOException, UnsupportedAudioFileException, LineUnavailableException {
		URL url = this.getClass().getClassLoader().getResource(sound);
		AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);

		Clip clip = AudioSystem.getClip();
		clip.open(audioIn);
		clip.start();
	}
}
