package systems.kinau.screenshare.utils;

import lombok.Getter;
import lombok.Setter;

import java.awt.*;

public class MonitorDimension extends Dimension {

    @Getter @Setter private Point start;

    public MonitorDimension(int x, int y, double width, double height) {
        setSize(width, height);
        setStart(new Point(x, y));
    }
}
