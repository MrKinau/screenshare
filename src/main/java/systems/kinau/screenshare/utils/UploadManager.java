package systems.kinau.screenshare.utils;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import javafx.application.Platform;
import lombok.Getter;
import org.controlsfx.dialog.ProgressDialog;
import systems.kinau.screenshare.ScreenShare;
import systems.kinau.screenshare.monitor.ErrorMonitor;
import systems.kinau.screenshare.monitor.UploadMonitor;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class UploadManager {

	private int failedUploads = 0;
	private SoundManager soundManager;
	@Getter private Service pasteService;

	public UploadManager(SoundManager soundManager, Service pasteService) {
		this.soundManager = soundManager;
		this.pasteService = pasteService;
	}

	public boolean sendFile(String fileName, boolean showDialog) {
		int port = 22;
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			JSch jsch = new JSch();

//			jsch.setKnownHosts(pasteService.getSshDir() + "known_hosts");
			jsch.addIdentity(pasteService.getSshDir() + "id_rsa");

			session = jsch.getSession(pasteService.getUsername(), pasteService.getHostname(), port);

			session.setConfig("StrictHostKeyChecking", "no");

			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();

			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(pasteService.getDirectory());
			File f = new File(fileName);

			if(showDialog) {
				UploadMonitor uploadMonitor = new UploadMonitor(f.length());
				Platform.setImplicitExit(false);
				Platform.runLater(() -> {
					new ProgressDialog(uploadMonitor);

                    Thread thread = new Thread(uploadMonitor);
                    thread.setDaemon(true);
                    thread.start();
				});
				channelSftp.put(new FileInputStream(f), f.getName(), uploadMonitor);
			} else
				channelSftp.put(new FileInputStream(f), f.getName());
			failedUploads = 0;
		} catch (Exception ex) {
			ex.printStackTrace();
			if(failedUploads <= 2) {
				failedUploads++;
				try {
					soundManager.playSound("error.wav");
					new ErrorMonitor(ex);
				} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
					e.printStackTrace();
				}
				sendFile(fileName, showDialog);
			}
		} finally {
			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
			if(failedUploads == 0)
				return true;
			return false;
		}
	}
}
