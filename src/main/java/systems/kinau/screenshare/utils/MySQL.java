package systems.kinau.screenshare.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQL {

    private Connection conn = null;

    public void openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection("jdbc:mysql://37.228.132.119/urlshortener?user=shortener&password=j7336nVFql");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addScreenEntry(String identifier, String redirection) {
        try {
            if (conn.isClosed())
                openConnection();
            String query = " insert into urls (urltag, redirection) values (?, ?)";

            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, identifier);
            preparedStatement.setString(2, redirection);
            preparedStatement.execute();
            conn.close();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
