package systems.kinau.screenshare;

import systems.kinau.screenshare.utils.Service;
import systems.kinau.screenshare.utils.SoundManager;
import systems.kinau.screenshare.utils.UploadManager;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileNotFoundException;

public class UploadSystem {

    private String path;
    private UploadManager uploadManager;

    public UploadSystem(String[] args) throws Exception {
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i < args.length; i++)
            sb.append(args[i]).append(" ");
        this.path = sb.toString();
        com.sun.javafx.application.PlatformImpl.startup(()->{});
        this.uploadManager = new UploadManager(new SoundManager(), Service.valueOf(args[0]));

        File file = new File(path);
        if(!file.exists())
            throw new FileNotFoundException(path + " existiert nicht!");
        uploadManager.sendFile(path, true);
        getLink();
        com.sun.javafx.application.PlatformImpl.exit();
        Thread.sleep(500);
    }

    private void getLink() {
        String link = uploadManager.getPasteService().getBaseURL() + new File(path).getName();
        StringSelection stringSelection = new StringSelection(link);
        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
        clpbrd.setContents(stringSelection, null);
    }


}
