package systems.kinau.screenshare.gui;

import lombok.Getter;
import lombok.Setter;
import systems.kinau.screenshare.ScreenShare;
import systems.kinau.screenshare.listener.AreaDraggedSelectionListener;
import systems.kinau.screenshare.listener.AreaSelectionListener;
import systems.kinau.screenshare.utils.MonitorDimension;
import systems.kinau.screenshare.utils.Service;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class CaptureArea extends JPanel {

	@Getter private ScreenCaptureGUI parent;
	@Getter @Setter private Point captureStartPoint;
	@Getter @Setter private Point captureEndPoint;
	@Getter @Setter private Point currentCaptureEndPoint;

	public CaptureArea(ScreenShare screenShare, ScreenCaptureGUI parent) {
		this.parent = parent;
		setBackground(new Color(0,0,0,1));
		setSize(screenShare.getMonitorDimension());

		addMouseListener(new AreaSelectionListener(this));
		addMouseMotionListener(new AreaDraggedSelectionListener(this));
	}

	@Override
	public void repaint() {
		super.repaint();
		if(getGraphics() == null)
			return;
		Graphics2D g2d = (Graphics2D) getGraphics();
		g2d.setBackground(new Color(0,0,0,1));
		g2d.clearRect(0,0,getWidth(),getHeight());
		g2d.setColor(getParent().getMode() == ScreenCaptureGUI.Mode.SCREENSHOT ? Color.CYAN : Color.RED);
		if(captureStartPoint != null && captureEndPoint == null && currentCaptureEndPoint != null)
			g2d.drawRect(getStart(captureStartPoint.x, currentCaptureEndPoint.x), getStart(captureStartPoint.y, currentCaptureEndPoint.y), getDifference(currentCaptureEndPoint.x, captureStartPoint.x), getDifference(currentCaptureEndPoint.y, captureStartPoint.y));
		else if(captureStartPoint != null && captureEndPoint != null) {
			g2d.drawRect(getStart(captureStartPoint.x, captureEndPoint.x), getStart(captureStartPoint.y, captureEndPoint.y), getDifference(captureEndPoint.x, captureStartPoint.x), getDifference(captureEndPoint.y, captureStartPoint.y));
			try {
				finishDrawing();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void finishDrawing() throws IOException {
		MonitorDimension monitorDimension = getParent().getScreenShare().getMonitorDimension();
		Rectangle bounds = new Rectangle(
				getStart(captureStartPoint.x + monitorDimension.getStart().x, captureEndPoint.x + monitorDimension.getStart().x) + 1,
				getStart(captureStartPoint.y + monitorDimension.getStart().y, captureEndPoint.y + monitorDimension.getStart().y) + 1,
				getDifference(captureStartPoint.x + monitorDimension.getStart().x, captureEndPoint.x + monitorDimension.getStart().x) - 1,
				getDifference(captureStartPoint.y + monitorDimension.getStart().y, captureEndPoint.y + monitorDimension.getStart().y) - 1
		);
		getParent().getScreenShare().centerMouse();
		if(getParent().getMode() == ScreenCaptureGUI.Mode.SCREENSHOT) {
			getParent().getScreenShare().takePicture(bounds);
			getParent().setVisible(false);
			getParent().getScreenShare().uploadFile(getParent().getScreenShare().getLastUUID().toString().replace("-","") + ".png", false, Service.NEBORIA);
			getParent().getScreenShare().copyLinkToClipboard(getParent().getScreenShare().getLastIdentifier());
			getParent().getScreenShare().addRediretion(getParent().getScreenShare().getLastIdentifier(), getParent().getScreenShare().getLastUUID().toString().replace("-","") + ".png", Service.NEBORIA);
		} else if(getParent().getMode() == ScreenCaptureGUI.Mode.RECORD) {
			getParent().getScreenShare().startRecording(bounds);
			getParent().setVisible(false);
		}
		setCaptureStartPoint(null);
		setCaptureEndPoint(null);
		setCurrentCaptureEndPoint(null);
	}

	public int getDifference(int i1, int i2) {
		int max = Math.max(i1, i2);
		int min = Math.min(i1, i2);
		return max - min;
	}

	public int getStart(int i1, int i2) {
		return Math.min(i1, i2);
	}
}
