package systems.kinau.screenshare.gui;

import lombok.Getter;
import lombok.Setter;
import systems.kinau.screenshare.ScreenShare;

import javax.swing.*;
import java.awt.*;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class ScreenCaptureGUI extends JWindow {

	private JPanel panel;
	@Getter 		public ScreenShare screenShare;
	@Getter @Setter public Mode mode;


	public ScreenCaptureGUI(ScreenShare screenShare){
		super();
		this.screenShare = screenShare;
		this.mode = Mode.SCREENSHOT;
		this.setSize(screenShare.getMonitorDimension());
		this.setLocation(screenShare.getMonitorDimension().getStart());

		setBackground(new Color(0,0,0,1));

		setAlwaysOnTop(true);

		setLayout(null);

		panel = new CaptureArea(screenShare, this);
		this.add(panel);
	}

	public enum Mode {
		SCREENSHOT, RECORD;
	}

}
