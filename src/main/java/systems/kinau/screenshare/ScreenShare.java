package systems.kinau.screenshare;


import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import systems.kinau.screenshare.gui.ScreenCaptureGUI;
import systems.kinau.screenshare.listener.ShortcutListener;
import systems.kinau.screenshare.monitor.ErrorMonitor;
import systems.kinau.screenshare.utils.*;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class ScreenShare {

	@Getter private Robot robot;
	@Getter public MySQL mySQL;
	@Getter public ScreenCaptureGUI screenCaptureGUI;
	@Getter public SoundManager soundManager;
	@Getter @Setter public UUID lastUUID;
	@Getter @Setter public String lastIdentifier;
	@Getter public MonitorDimension monitorDimension = getScreen();
	@Getter @Setter public boolean recording = false;
	@Getter @Setter public RecordUtils recordUtils = new RecordUtils();

	public ScreenShare() {
		try {
			this.mySQL = new MySQL();
			this.mySQL.openConnection();
			this.robot = new Robot();
			this.screenCaptureGUI = new ScreenCaptureGUI(this);
			this.soundManager = new SoundManager();

			Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
			logger.setLevel(Level.WARNING);

			com.sun.javafx.application.PlatformImpl.startup(()->{});

			GlobalScreen.registerNativeHook();
//			new Thread(() -> KinauInfo.main(new String[]{"-ip", "kinau.systems"})).start();
			GlobalScreen.addNativeKeyListener(new ShortcutListener(this));
		} catch (AWTException | NativeHookException e) {
			e.printStackTrace();
		}
	}

	public void takePicture(Rectangle section) throws IOException {
		BufferedImage image = getScreenPicture(section);
		File file = new File(System.getenv("appdata") + "/ScreenShare/");
		file.mkdirs();
		lastUUID = UUID.randomUUID();
		lastIdentifier = RandomStringUtils.randomAlphanumeric(8);
		ImageIO.write(image, "png", new File(file.getPath() + "/" + lastUUID.toString().replace("-","") + ".png"));
	}

	public void uploadFile(String name, boolean showDialog, Service service) {
		service.getUploadManager(getSoundManager()).sendFile(System.getenv("appdata") + "/ScreenShare/" + name, showDialog);
		try {
			getSoundManager().playSound("pushed.wav");
		} catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
			e.printStackTrace();
		}
		new Thread(() -> {
            try {
                Thread.sleep(1000);
                deleteFiles(new File(System.getenv("appdata") + "/ScreenShare/"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
	}

	public void copyLinkToClipboard(String identifier) {
		String link = "https://screens.neboria.eu/" + getLastUUID().toString().replace("-","") + ".png";
		StringSelection stringSelection = new StringSelection(link);
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(stringSelection, null);
	}

	public void addRediretion(String identifier, String value, Service pasteService) {
//		getMySQL().addScreenEntry(identifier, pasteService.getBaseURL() + value);
	}

	public void deleteFiles(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteFiles(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
	}

	private MonitorDimension getScreen() {
		int width = 0, height = 0;
		int x = 0, y = 0;
		for (GraphicsDevice graphicsDevice : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
			if(x > graphicsDevice.getDefaultConfiguration().getBounds().x && graphicsDevice.getDefaultConfiguration().getBounds().x < 0)
				x = graphicsDevice.getDefaultConfiguration().getBounds().x;
			if((y > graphicsDevice.getDefaultConfiguration().getBounds().y && graphicsDevice.getDefaultConfiguration().getBounds().y < 0))
				y = graphicsDevice.getDefaultConfiguration().getBounds().y;
			if((Math.abs(graphicsDevice.getDefaultConfiguration().getBounds().getX()) + graphicsDevice.getDefaultConfiguration().getBounds().getWidth()) > width)
				width += graphicsDevice.getDisplayMode().getWidth();
			if((Math.abs(graphicsDevice.getDefaultConfiguration().getBounds().getY()) + graphicsDevice.getDefaultConfiguration().getBounds().getHeight()) > height)
				height += graphicsDevice.getDisplayMode().getHeight();
		}
		height = 2160;
		return new MonitorDimension(x, y, width, height);
	}

	public BufferedImage getScreenPicture(Rectangle section) {
		return getRobot().createScreenCapture(section);
	}

	public void startRecording(Rectangle bounds) {
		this.recordUtils = new RecordUtils();
		recordUtils.startRecord(this, 120, bounds);
	}

	public void centerMouse() {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();

		Point p = MouseInfo.getPointerInfo().getLocation();

		for (GraphicsDevice device: gs) {
			GraphicsConfiguration[] configurations = device.getConfigurations();
			for (GraphicsConfiguration config: configurations) {
				Rectangle bounds = config.getBounds();
				if(bounds.contains(p)) {
					Point b = new Point((int) (bounds.x + (bounds.width / 2.0D)), (int) (bounds.y + (bounds.height / 2.0D)));

					try {
						Robot r = new Robot(device);
						r.mouseMove(b.x, b.y);
					} catch (AWTException e) {
						e.printStackTrace();
					}
					return;
				}
			}
		}
	}

	public static void main(String[] args) {
		if(args.length == 0)
			new ScreenShare();
		else {
			try {
				new UploadSystem(args);
			} catch (Exception e) {
				new ErrorMonitor(e);
				e.printStackTrace();
			}
		}

	}
}
