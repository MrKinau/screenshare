package systems.kinau.screenshare.listener;

import systems.kinau.screenshare.gui.CaptureArea;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class AreaSelectionListener implements MouseListener {

	private CaptureArea screenCaptureArea;

	public AreaSelectionListener(CaptureArea captureArea) {
		this.screenCaptureArea = captureArea;
	}

	public void mouseClicked(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		screenCaptureArea.setCaptureStartPoint(e.getPoint());
		screenCaptureArea.repaint();
	}

	public void mouseReleased(MouseEvent e) {
		screenCaptureArea.setCaptureEndPoint(e.getPoint());
		screenCaptureArea.repaint();
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}
}
