package systems.kinau.screenshare.listener;

import lombok.Getter;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import systems.kinau.screenshare.ScreenShare;
import systems.kinau.screenshare.gui.ScreenCaptureGUI;
import systems.kinau.screenshare.utils.Service;

import java.io.IOException;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class ShortcutListener implements NativeKeyListener {

	@Getter private ScreenShare screenShare;

	public ShortcutListener(ScreenShare screenShare) {
		this.screenShare = screenShare;
	}


	public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {}

	public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
		if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F7) {
			if (!screenShare.getScreenCaptureGUI().isVisible())
				screenShare.getScreenCaptureGUI().setVisible(true);
			screenShare.getScreenCaptureGUI().setMode(ScreenCaptureGUI.Mode.SCREENSHOT);
		} else if(nativeKeyEvent.getKeyCode() == NativeKeyEvent.VC_F4) {
			if (!screenShare.getScreenCaptureGUI().isVisible() && !screenShare.getRecordUtils().isRunning())
				screenShare.getScreenCaptureGUI().setVisible(true);
			else if(screenShare.getRecordUtils().isRunning()) {
				screenShare.getRecordUtils().stop();
				try {
					screenShare.getRecordUtils().save(true);
					screenShare.uploadFile(screenShare.getLastUUID().toString().replace("-","") + ".gif", true, Service.NEBORIA);
					screenShare.copyLinkToClipboard(screenShare.getLastIdentifier());
					screenShare.addRediretion(screenShare.getLastIdentifier(), screenShare.getLastUUID().toString().replace("-","") + ".gif", Service.NEBORIA);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			screenShare.getScreenCaptureGUI().setMode(ScreenCaptureGUI.Mode.RECORD);
		}
	}

	public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {

	}
}
