package systems.kinau.screenshare.listener;

import systems.kinau.screenshare.gui.CaptureArea;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

/**
 * Copyright (c) David Lüdtke 2017. All rights reserved
 * 31.01.2017
 */
public class AreaDraggedSelectionListener implements MouseMotionListener {

	private CaptureArea captureArea;

	public AreaDraggedSelectionListener(CaptureArea captureArea) {
		this.captureArea = captureArea;
	}

	public void mouseDragged(MouseEvent e) {
		captureArea.setCurrentCaptureEndPoint(e.getPoint());
		captureArea.repaint();
	}

	public void mouseMoved(MouseEvent e) {

	}
}
