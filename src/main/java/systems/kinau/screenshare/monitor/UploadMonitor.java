package systems.kinau.screenshare.monitor;

import com.jcraft.jsch.SftpProgressMonitor;
import javafx.concurrent.Task;
import lombok.Getter;
import lombok.Setter;

public class UploadMonitor extends Task implements SftpProgressMonitor{

    @Getter private long bytes;
    @Getter @Setter private long sentBytes = 0;

    private boolean running;

    public UploadMonitor(long bytes) {
        this.bytes = bytes;
        this.running = true;
    }

    @Override
    public void init(int i, String s, String s1, long l) { }

    @Override
    public boolean count(long l) {
        setSentBytes(getSentBytes() + l);
        return true;
    }

    @Override
    public void end() {
        this.running = false;
    }

    @Override
    protected Object call() throws Exception {
        while (running) {
            updateProgress(getSentBytes(), getBytes());
            updateMessage("Uploading (" + (getSentBytes() / 1024) + "/" + (getBytes() / 1024) + ")");
            Thread.sleep(50);
        }
        cancel();
        return null;
    }
}
