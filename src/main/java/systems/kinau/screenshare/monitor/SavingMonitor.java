package systems.kinau.screenshare.monitor;

import javafx.concurrent.Task;
import lombok.Getter;

public class SavingMonitor extends Task {

    @Getter private int imagesToSave, imagesSaved;

    private boolean running = true;

    public SavingMonitor(int imagesToSave) {
        this.imagesToSave = imagesToSave;
    }

    public void setImagesSaved(int imagesSaved) {
        this.imagesSaved = imagesSaved;
        if(getImagesSaved() >= getImagesToSave())
            running = false;
    }

    @Override
    protected Object call() throws Exception {
        while (running) {
            updateProgress(getImagesSaved(), getImagesToSave());
            updateMessage("Saving (" + getImagesSaved() + "/" + getImagesToSave() + ")");
            Thread.sleep(50);
        }
        cancel();
        return null;
    }
}
